package com.logger;

import com.logger.services.RandomException;
import com.logger.services.RandomNumberService;
import org.apache.log4j.Logger;

public class Main {

    private static final String MSG_SUCCESSFUL_COMPLETE = "Application launched successfully.";

    private static final Logger LOG_CONSOLE = Logger.getLogger("loggerConsole");
    private static final Logger LOG_FILE = Logger.getLogger("loggerFile");

    public static void main(String[] args) {

        try {

            int randomNumber = new RandomNumberService().getRandomNumber();
            LOG_CONSOLE.info(MSG_SUCCESSFUL_COMPLETE);
            LOG_FILE.info(MSG_SUCCESSFUL_COMPLETE);

        } catch(RandomException ex) {

            LOG_CONSOLE.error(ex.getMessage() + ex.getNumber());
            LOG_FILE.error(ex.getMessage() + ex.getNumber());

        }

    }
}
