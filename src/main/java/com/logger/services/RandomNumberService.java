package com.logger.services;

import java.util.Random;

public class RandomNumberService {

    Random random = new Random();

    public int getRandomNumber() throws RandomException {

        int randomNumber = random.nextInt(11);

        if (randomNumber <= 5) {
            throw new RandomException("Generated number - ", randomNumber);
        }

        return randomNumber;
    }

}
