package com.logger.services;

public class RandomException extends Exception{

    private int number;

    public RandomException(String message, int num) {

        super(message);
        number = num;

    }

    public int getNumber() {
        return number;
    }

}
